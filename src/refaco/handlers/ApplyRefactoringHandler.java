package refaco.handlers;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import refaco.RefactoringData;
import refaco.RefactoringOperationStatus;
import refaco.RowData;
import refaco.exceptions.RefactoringException;
import refaco.refactorings.ExtractClassRefactoring;
import refaco.refactorings.ExtractMethodRefactoring;
import refaco.refactorings.IntroduceParameterObjectRefactoring;
import refaco.refactorings.MoveMethodRefactoring;
import refaco.refactorings.RemoveParametersRefactoring;
import refaco.views.CodeSmellTableView;
import refaco.views.RefactoringListTableView;

/**
 * This class manage the apply of the refactorings
 *
 */
public class ApplyRefactoringHandler extends AbstractHandler {
	
	int refactoringIndex;				// the current refactoring
	private Thread threadObject;	// thread for synchronize the runnable with the next button

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// Get Refactoring Sequence view
		RefactoringListTableView refactoringListTV = (RefactoringListTableView) HandlerUtil
				.getActiveWorkbenchWindow(event).getActivePage().findView(RefactoringListTableView.ID);
		// Get the checked refactorings in the table
		Object[] selected = refactoringListTV.getTableViewer().getCheckedElements();
		List<RefactoringData> selectedRefactoring = new ArrayList<RefactoringData>();
		List<RefactoringOperationStatus> refactoringStatus = new ArrayList<RefactoringOperationStatus>();
		for (Object o : selected) {
			selectedRefactoring.add(((RowData) o).getRefactoringData());
		}
		if (selectedRefactoring != null) {
			// get the project data from Code Smell view
			CodeSmellTableView codeSmellTV = (CodeSmellTableView) HandlerUtil.getActiveWorkbenchWindow(event)
					.getActivePage().findView(CodeSmellTableView.ID);
			String projectName = codeSmellTV.getLastProjectAnalized();
			refactoringIndex = 0;			

			// Runnable task
			IRunnableWithProgress runnable = new IRunnableWithProgress() {
				public void run(IProgressMonitor monitor) throws InterruptedException {
					int totalUnitsOfWork = selectedRefactoring.size();
					monitor.beginTask("Running RefACo", totalUnitsOfWork);
					int cont = 1;
					for (RefactoringData r : selectedRefactoring) {
						monitor.subTask("Apply Code refactoring " + cont + " de " + totalUnitsOfWork);
						
						// wait for the user click next button
						if(refactoringIndex < cont){
							synchronized(threadObject){
                                try{
                                    threadObject.wait();
                                } 
                                catch (InterruptedException e) {}
                            }
						}
						// if the user has cancelled the operation finish
						if(monitor.isCanceled()){
							return;
						}
						monitor.worked(1);
						monitor.subTask("Applying Code refactoring " + cont + " de " + totalUnitsOfWork);
						refactoringStatus.add(applyRefactoring(r, projectName));
						cont++;
					}
					monitor.done();
				}
			};
			threadObject = new Thread();
	        threadObject.start();
			// Create the monitor dialog
			IWorkbenchWindow win = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			Shell shell = win != null ? win.getShell() : null;
			try {
				new ProgressMonitorDialog(shell) {
					@Override
					public Composite createDialogArea(Composite parent) {
						Composite container = (Composite) super.createDialogArea(parent);
						setCancelable(true);
						return container;
					}
					
					@Override
					protected void createButtonsForButtonBar(Composite parent) {
						// create the next button
						super.createButtonsForButtonBar(parent);
						createButton(parent, IDialogConstants.OK_ID, "Next", false); 
					}
					
					@Override
					protected void buttonPressed(int buttonId) {
						if(buttonId == IDialogConstants.OK_ID){
							refactoringIndex++;
							// Resume (next is pressed)
			                synchronized(threadObject)
			                {
			                    threadObject.notify();
			                }
						}else if(buttonId == IDialogConstants.CANCEL_ID){
							// Cancel is pressed
							cancelPressed();
							this.getProgressMonitor().setCanceled(true);
							synchronized(threadObject)
			                {
			                    threadObject.notify();
			                }
						}
					}
				}.run(true, true, runnable);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
				HandlersUtils.showError(e.getMessage());
			} catch (InterruptedException e) {
				e.printStackTrace();
				HandlersUtils.showError(e.getMessage());
			}
			
		}
		return null;
	}

	/** This method apply a refactoring operation 
	 * 
	 * @param refactoringData The refactoring
	 * @param projectName	  The project name	
	 * @return
	 */
	private RefactoringOperationStatus applyRefactoring(RefactoringData refactoringData, String projectName) {
		RefactoringOperationStatus status = new RefactoringOperationStatus();
		try {
			switch (refactoringData.getRefactoringType()) {
			case INTRODUCE_PARAMETER_OBJECT:
				IntroduceParameterObjectRefactoring ipoR = new IntroduceParameterObjectRefactoring(refactoringData,
						projectName);
				ipoR.apply();
				break;
			case  REMOVE_PARAMETER:
				RemoveParametersRefactoring rpR = new RemoveParametersRefactoring(refactoringData, projectName);
				rpR.apply();
				break;
			case MOVE_METHOD:
				MoveMethodRefactoring mmR = new MoveMethodRefactoring(refactoringData, projectName);
				mmR.apply();
				break;
			case EXTRACT_CLASS:
				ExtractClassRefactoring ecR = new ExtractClassRefactoring(refactoringData, projectName);
				ecR.apply();
				break;
			case EXTRACT_METHOD:
				String selection = refactoringData.getMethodTarget();
				// selection format: firstLine:numLines
				refactoringData.setSelectionStart(Integer.parseInt(selection.substring(0, selection.indexOf(':'))));
				refactoringData.setSelectionLength(Integer.parseInt(selection.substring(selection.indexOf(':') + 1, selection.length())));
				ExtractMethodRefactoring emR = new ExtractMethodRefactoring(refactoringData, projectName);
				emR.apply();
				break;
			default:
				System.err.println("Refactoring: " + refactoringData.getRefactoringType() + " not implemented");
				status.setCode(-1);
				status.setMessage("Refactoring: " + refactoringData.getRefactoringType() + " not implemented");
				return status;
			}
		} catch (RefactoringException e) {
			e.printStackTrace();
			status.setCode(-1);
			status.setMessage(e.getMessage());
			HandlersUtils.showError(e.getMessage());
			return status;
		}
		status.setCode(0);
		status.setMessage("Task completed");
		return status;
	}
}
