package refaco.refactorings;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.internal.corext.codemanipulation.CodeGenerationSettings;
import org.eclipse.jdt.internal.corext.refactoring.structure.MoveInstanceMethodProcessor;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import refaco.RefactoringData;
import refaco.exceptions.RefactoringException;

/**
* Move Method refactoring
*
*/
public class MoveMethodRefactoring extends refaco.refactorings.Refactoring {

	public MoveMethodRefactoring(RefactoringData _refactoringData, String _projectName) {
		super(_refactoringData, _projectName);
	}

	public void apply() throws RefactoringException {
		
		// Get the package and class name (Source)
		String temp = getRefactoringData().getClassSource();
		int index = temp.lastIndexOf('.');
		String packageSourceName = temp.substring(0, index);
		String classSourceName = temp.substring(index + 1, temp.length());
		
		// Get the method name and parameters
		String methodAndParameters = getRefactoringData().getMethodTarget();
		String methodName = null;
		String[] parameters = null;
		try{
			methodName = methodAndParameters.substring(0, methodAndParameters.indexOf('(')).replaceAll("\\s","");
			if(methodName.equals("<init>")){
				methodName = classSourceName;
			}
			parameters = methodAndParameters.substring(methodAndParameters.indexOf('(') +1,methodAndParameters.indexOf(')')).split(",");
			if(parameters.length == 1 && parameters[0].length()==0)
				parameters = null;
		}catch(StringIndexOutOfBoundsException e){
			throw new RefactoringException("Method name format exception");
		}
		
		
		// Get the package and class name (Target)
		temp = getRefactoringData().getClassTarget();
		index = temp.lastIndexOf('.');
		String classTargetName = temp.substring(index + 1, temp.length());

		// Get the IProject from the projectName
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject project = root.getProject(getProjectName());
		
		try {
			if (project.isNatureEnabled("org.eclipse.jdt.core.javanature")) {
				
				// Get the IType
				IJavaProject javaProject = JavaCore.create(project);
				IPackageFragmentRoot rootpackage = javaProject.getPackageFragmentRoot(project.getFolder("src"));
				IPackageFragment classPackage = rootpackage.getPackageFragment(packageSourceName);
				ICompilationUnit classCU = classPackage.getCompilationUnit(classSourceName + ".java");
				IType typeSource = classCU.getType(classSourceName);
				
				// Get the IMethod
				IMethod[] methods = typeSource.getMethods();
				IMethod method = null;
				int i = 0;
				while(method == null && i < methods.length){
					IMethod me = methods[i];
					if (me.getElementName().equals(methodName) && (parameters == null && me.getNumberOfParameters()==0)
							|| (parameters!=null && me.getNumberOfParameters() == parameters.length)) {
						method = me;
					}
					i++;
				}
				
				if (method != null && method.exists()) {
					
					// Create the classes needed for apply the refactoring
					MoveInstanceMethodProcessor processor = new MoveInstanceMethodProcessor(method, new CodeGenerationSettings());
					Refactoring refactoring = new ProcessorBasedRefactoring(processor);
					// Set the target class
					refactoring.checkInitialConditions(null);
					IVariableBinding[] targets = processor.getPossibleTargets();
					IVariableBinding targetArgument = null;
					for(IVariableBinding target: targets){
						if(target.getType().getName().equals(classTargetName)){
							processor.setTarget(target);
							targetArgument = target;
							break;
						}
					}
					refaco.refactorings.MoveInstanceMethodWizard wizard = new refaco.refactorings.MoveInstanceMethodWizard(processor, refactoring, targetArgument);
					RefactoringWizardOpenOperation op = new RefactoringWizardOpenOperation(wizard);
					
					// Execute the refactoring
					Display.getDefault().syncExec(new Runnable(){
						@Override
						public void run() {
							try {
								op.run(Display.getDefault().getActiveShell(), "");
							} catch (InterruptedException e) {
								e.printStackTrace();
								// operation was cancelled
							}
						}
					});
						
				} else {
					throw new RefactoringException("Method not exist");
				}
			} else {
				System.err.println("Nature disabled");
				throw new RefactoringException("Java Nature disabled");
			}
		} catch (CoreException e1) {
			e1.printStackTrace();
			throw new RefactoringException(e1.getMessage());
		} 
	}
	
}
