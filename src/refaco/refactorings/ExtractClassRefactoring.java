package refaco.refactorings;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.refactoring.IJavaRefactorings;
import org.eclipse.jdt.core.refactoring.descriptors.ExtractClassDescriptor;
import org.eclipse.jdt.core.refactoring.descriptors.ExtractClassDescriptor.Field;
import org.eclipse.jdt.internal.ui.refactoring.*;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringContribution;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import refaco.RefactoringData;
import refaco.exceptions.RefactoringException;

/**
* Extract Class refactoring
*
*/
public class ExtractClassRefactoring extends refaco.refactorings.Refactoring {

	public ExtractClassRefactoring(RefactoringData _refactoringData, String _projectName) {
		super(_refactoringData, _projectName);
	}

	public void apply() throws RefactoringException {
		
		// Get the package and class name (Source)
		String temp = getRefactoringData().getClassSource();
		int index = temp.lastIndexOf('.');
		String packageSourceName = temp.substring(0, index);
		String classSourceName = temp.substring(index + 1, temp.length());

		// Get the IProject from the projectName
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject project = root.getProject(getProjectName());
		
		try {
			if (project.isNatureEnabled("org.eclipse.jdt.core.javanature")) {
				
				// Get the IType
				IJavaProject javaProject = JavaCore.create(project);
				IPackageFragmentRoot rootpackage = javaProject.getPackageFragmentRoot(project.getFolder("src"));
				IPackageFragment classPackage = rootpackage.getPackageFragment(packageSourceName);
				ICompilationUnit classCU = classPackage.getCompilationUnit(classSourceName + ".java");
				IType typeSource = classCU.getType(classSourceName);
				
				// Get the name of all the fields
				List<String> nameFields = getRefactoringData().getFields();

				if (typeSource != null) {

					// Initialize the refactoring descriptor
					RefactoringContribution contribution = RefactoringCore
							.getRefactoringContribution(IJavaRefactorings.EXTRACT_CLASS);
					ExtractClassDescriptor descriptor = (ExtractClassDescriptor) contribution.createDescriptor();
					descriptor.setProject(javaProject.getElementName());
					descriptor.setPackage(classPackage.getElementName());
					descriptor.setType(typeSource);
					
					boolean atLeastOne = false;
					// Set the fields in the descriptor
					Field[] allFields = ExtractClassDescriptor.getFields(typeSource);
					for(int i =0; i < allFields.length; i++){
						if(nameFields.contains(allFields[i].getFieldName())){
							allFields[i].setCreateField(true);
							atLeastOne = true;
						}else{
							allFields[i].setCreateField(false);
						}
					}
					if(atLeastOne){
						
					
					
					descriptor.setFields(allFields);

					// Create the classes needed for apply the refactoring
					Refactoring refactoring = new org.eclipse.jdt.internal.corext.refactoring.structure.ExtractClassRefactoring(descriptor);
					ExtractClassWizard wizard = new ExtractClassWizard(descriptor, refactoring);
					RefactoringWizardOpenOperation op = new RefactoringWizardOpenOperation(wizard);
					
					// Execute the refactoring
					op.run(Display.getDefault().getActiveShell(), "");
					}else{
						throw new RefactoringException("Fields doesn't exist");
					}
				} else {
					System.err.println("Class Empty");
					throw new RefactoringException("Class Empty");
				}
			} else {
				System.err.println("Nature disabled");
				throw new RefactoringException("Java Nature disabled");
			}
		} catch (CoreException e1) {
			e1.printStackTrace();
			throw new RefactoringException(e1.getMessage());
		}
		catch (InterruptedException e) {
			e.printStackTrace();
			// operation was cancelled
			throw new RefactoringException("Operation was cancelled");
		}
	}

}
